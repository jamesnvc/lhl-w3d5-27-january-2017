//
//  ViewController.m
//  MediaStuff
//
//  Created by James Cash on 27-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *chosenImageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pickImage:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];

    // Need to make ourselves the delegate of the picker so we can see what it picked
    picker.delegate = self;

    // Need to check what source types are supported on the device we're running on
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        // Simulator doesn't have a camera, so we need to check this - if we didn't this would crash in the simulator
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }

    NSArray<NSString*> *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    NSLog(@"Media types: %@", mediaTypes);

    [self presentViewController:picker animated:YES completion:^{
        NSLog(@"Picker is shown now");
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    NSLog(@"Cancelled picking image");
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSLog(@"Picked %@", info);
    self.chosenImageView.image = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        NSLog(@"Made picker go away");
    }];
}

@end
